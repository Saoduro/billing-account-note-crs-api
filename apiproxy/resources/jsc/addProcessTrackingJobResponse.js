var response = context.getVariable("xmlResponse.APTJbody");
var responseCode = context.getVariable("xmlResponse.responseCode");
var asOfDateTime = context.getVariable("xmlResponse.asOfDateTime");

if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);

    var correlationId = context.getVariable("correlationId");
    
    var responseObject = {};

    if (responseCode == 'Success') {
        
        var asOfDateTime = context.getVariable("xmlResponse.asOfDateTime");
        var processTrackingJobNumber = responseBody.ProcessTrackingNumber.toString();
    
        //CRS does not speak milliseconds, so we add default milliseconds of .000 to follow our standard 
        var asOfDateTimePlusMS = asOfDateTime.concat(".000");
        
        context.setVariable('response.status.code', 201);

        // map correlationId
        if (correlationId != "NULL") {
            responseObject.correlationId = correlationId;
        } else {
            responseObject.correlationId = "";
        }
        
        //map note
        var notesArray = [];
        var noteObject = {};
        
        // map confirmationNoteId
        if (processTrackingJobNumber != "NULL") {
            noteObject.confirmationNoteId = processTrackingJobNumber;
        } else {
            noteObject.confirmationNoteId = "";
        }
        
        // add note object to notes array
        notesArray.push(noteObject);
        
        // add note object to notes array to response object
        responseObject.notes = notesArray;
       

    } else {
        context.setVariable('response.status.code', 404);
        
        var errorDetail = context.getVariable("xmlResponse.errorDetail");
        var errorAsOfDateTime = context.getVariable("xmlResponse.errorAsOfDateTime");
        context.setVariable("isValidResponse", false);
        context.setVariable("responseMessage", errorDetail);
        context.setVariable("errorTS", errorAsOfDateTime);
    }


    context.setVariable("finalResponse", JSON.stringify(responseObject));


}