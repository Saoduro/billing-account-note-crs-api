var type = context.getVariable("type");
//Hash Table to Convert notes type to PTJ type, class, and subClass
var noteType = {
    "HOMESMART_ORDER": "SY/CUST/HE",
    "BANKACCT_VALIDATE": "SY/CUST/EW",
    "ADD_AUTOPAY": "BL/OMRP/AD ",
    "ADD_PAYMENT": "BL/OMPAY/AD",
    "ADD_MANUALPAY": "BL/OMEB/AD",
    "ADD_BANKACCOUNT": "BL/OBANK/AD",
    "ADD_PAYMENT": "BL/OMPAY/AD",
    "UPDATE_AUTOPAY": "BL/OMRP/UP",
    "UPDATE_MANUALPAY": "BL/OMEB/UP",
    "UPDATE_BANKACCOUNT": "BL/OBANK/DU",
    "UPDATE_PAYMENT": "BL/OMPAY/UP",
    "DELETE_AUTOPAY": "BL/OMRP/DL",
    "DELETE_MANUALPAY": "BL/OMEB/DL",
    "DELETE_BANKACCOUNT": "BL/OBANK/DL",
    "DELETE_PAYMENT": "BL/OMPAY/DL"
    
};

 
var PTJCodes = noteType[type];

context.setVariable("type", PTJCodes.split("/")[0]);
context.setVariable("class", PTJCodes.split("/")[1]);
context.setVariable("subClass", PTJCodes.split("/")[2]);
