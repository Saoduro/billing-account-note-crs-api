<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:param name="correlationId" select="''"/>
	<xsl:param name="currentSystemTimeStampCRS" select="''"/>
	<xsl:param name="billingId" select="''"/>
	<xsl:param name="type" select="''"/>
	<xsl:param name="class" select="''"/>
	<xsl:param name="subClass" select="''"/>
	<xsl:param name="description1" select="''"/>
	<xsl:param name="description2" select="''"/>
    <xsl:template match="*">
    <OperationCall>
                <Header>
                    <MessageID><xsl:value-of select="$correlationId"/></MessageID>
                    <MessageDateTime><xsl:value-of select="$currentSystemTimeStampCRS"/></MessageDateTime>
                    <APIName>ProcessTrackingMaintenance</APIName>
                    <OperationType>Sync</OperationType>
                    <OperationName>addProcessTrackingJob</OperationName>
                    <OperationVersion>1.2</OperationVersion>
                </Header>
                <Body>
                    <ProcessTrackingMaintenance>
                        <Sync>
                            <addProcessTrackingJob>
                                <v1.2>
                                    <Call>
                                        <CustomerNumber><xsl:value-of select="$billingId"/></CustomerNumber>
                                        <TypeCode><xsl:value-of select="$type"/></TypeCode>
                                        <ClassCode><xsl:value-of select="$class"/></ClassCode>
                                        <SubclassCode><xsl:value-of select="$subClass"/></SubclassCode>
                                        <StatusCode>C</StatusCode>
                                        <xsl:if test="string-length($description1) > 0">
                                        <Description1><xsl:value-of select="$description1"/></Description1>
                                        </xsl:if>
                                        <xsl:if test="string-length($description2) > 0">
                                        <Description2><xsl:value-of select="$description2"/></Description2>
                                        </xsl:if>
                                    </Call>
                                </v1.2>
                            </addProcessTrackingJob>
                        </Sync>
                    </ProcessTrackingMaintenance>
                </Body>
            </OperationCall>
            </xsl:template>
</xsl:stylesheet>